
$(function () {
  $('#m_aside_left_minimize_toggle,#m_aside_left_offcanvas_toggle').click(function () {
    $('body').toggleClass('m-brand--minimize').toggleClass('m-aside-left--minimize')
    $('#m_aside_left').toggleClass('m-aside-left--on')
  })
  $('#m_aside_header_menu_mobile_toggle').click(function () {
    $('#m_header_menu').toggleClass('m-aside-header-menu-mobile--on')
  })
  $('#m_aside_header_topbar_mobile_toggle').click(function () {
    $('body').toggleClass('m-topbar--on')
  })

  $('.m-body').click(function () {
    if ($('body').hasClass('m-aside-left--minimize') === false) {
      $('body').addClass('m-brand--minimize').addClass('m-brand--minimize')
    }

    if ($('#m_aside_left').hasClass('m-aside-left--on') || $('#m_aside_left').hasClass('m-aside-left--on')) {
      $('#m_aside_left').removeClass('m-aside-left--on')
    }

    if ($('#m_header_menu').hasClass('m-aside-header-menu-mobile--on')) {
      $('#m_header_menu').removeClass('m-aside-header-menu-mobile--on')
    }
    if ($('body').hasClass('m-topbar--on')) {
      $('body').removeClass('m-topbar--on')
    }
  })
})
