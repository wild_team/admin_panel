# Code Style

#### Follow eslint 指示調整
  1. tab = 2space
  2. 無 ; 符號

#### route-link 使用 name，不要使用 path
```
    *.vue
    <router-link :to="{ name: 'Dashboard' }">link</router-link>
    
    router.js
    { path: '*', redirect: { name: 'Dashboard' } }
    
    script tag
    this.$router.push({name: 'Dashboard'})
```

#### 檔案命名
  1. component 基本上使用 Metronic 的 BEM block 命名
  2. 整頁的 html 放置在 page folder，以 router "name" 來命名
    
#### CSS class 命名
  1. 有模組概念的，使用 [BEM](http://getbem.com/) 結構化命名
  2. Global 性質、單一個體，可以使用一般命名即可
  
    