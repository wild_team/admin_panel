# Dependency packages


###Theme
- Metronic 5.0 demo2 (bootstrap4)
  * [Download](https://bitbucket.org/wild_team/admin_panel/downloads/Metronic_v5.0_demo2.zip)
  * [Online Layout Builder](https://keenthemes.com/metronic/preview/?page=builder&demo=default)


###Vue Official
- create project parameter ( babel, eslint+standard, jest)
- vue-router
- vuex

###3rd Party Packages
- Webpack 4 (in vue-cli-serve)
- vue-meta (Change head title,meta, body class..)
