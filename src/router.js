import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

Vue.use(Router)
Vue.use(Meta)

// Vue.use(VueBreadcrumbs, {
//   template: '<nav class="breadcrumb" v-if="$breadcrumbs.length"> ' +
//   '<router-link class="breadcrumb-item" v-for="(crumb, key) in $breadcrumbs" :to="linkProp(crumb)" :key="key">{{ crumb | crumbText }}</router-link> ' +
//   '</nav>'
// })

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: resolve => require(['@/views/Login.vue'], resolve)
    },
    {
      path: '/',
      name: 'Frame',
      meta: { requiresAuth: false, breadcrumb: 'Home Page' },
      component: resolve => require(['@/components/frame/m-page.vue'], resolve),
      children: [
        {
          path: 'd',
          name: 'Device',
          meta: {
            breadcrumb: 'Foo Page'
          },
          component: resolve => require(['@/views/Device.vue'], resolve)
        },
        {
          path: 'dt',
          name: 'DeviceType',
          component: resolve => require(['@/views/DeviceType.vue'], resolve)
        },
        {
          path: 'dash',
          name: 'Dashboard',
          meta: { aaa: 'asdasd' },
          component: resolve => require(['@/views/Dashboard.vue'], resolve)
        },
        // { path: '*', redirect: { name: 'Dashboard' } }
        { path: '*', redirect: { name: 'Login' } }
      ]
    }
  ]
})
